<?php declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class ProjectImage
 * @package App\Entity
 * @Vich\Uploadable
 * @ORM\Entity(repositoryClass="App\Repository\ProjectImageRepository")
 */
class ProjectImage {
    use TimestampableEntity;

    /**
     * @var int|null
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $id;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="project_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @var Tag|null
     * @ORM\ManyToOne(targetEntity="App\Entity\Tag", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    protected $tag;

    /**
     * @var Project
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="projectImages")
     * @ORM\JoinColumn(referencedColumnName="id", name="project_id")
     */
    protected $project;

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void {
        $this->name = $name;
    }

    /**
     * @return Tag|null
     */
    public function getTag(): ?Tag {
        return $this->tag;
    }

    /**
     * @param Tag|null $tag
     */
    public function setTag(?Tag $tag): void {
        $this->tag = $tag;
    }

    /**
     * @return Project|null
     */
    public function getProject(): ?Project {
        return $this->project;
    }

    /**
     * @param Project|null $project
     */
    public function setProject(?Project $project): void {
        $this->project = $project;
    }

    public function getImageUrl() {
        return 'http://api.matfix.cz/images/'.$this->image;
    }

    public function getSampleImageUrl() {
        if ($this->getProject()->getSampleImage()) {
            return $this->getProject()->getSampleImageUrl();
        }

        return $this->getImageUrl();
    }
}
