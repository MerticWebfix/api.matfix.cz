<?php declare(strict_types=1);

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity()
 * @Vich\Uploadable
 */
class Project {

    use TimestampableEntity;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $id;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    protected $name;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    protected $projectWebUrl = null;

    /**
     * @var string|null
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(type="string", length=600, nullable=true)
     */
    protected $url;

    /**
     * @var DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $date;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=5000, nullable=true)
     */
    protected $description;

    /**
     * @var ArrayCollection|ProjectImage[]
     * @OneToMany(targetEntity="App\Entity\ProjectImage", mappedBy="project", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $projectImages;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="main_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string|null
     */
    private $sampleImage;

    /**
     * @Vich\UploadableField(mapping="sample_images", fileNameProperty="sampleImage")
     * @var File
     */
    private $sampleImageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string|null
     */
    private $seoTitle = null;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     * @var string|null
     */
    private $seoDesc = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string|null
     */
    private $seoKeyWords = null;

    /**
     * Project constructor.
     */
    public function __construct() {
        $this->projectImages = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string {
        return $this->url;
    }

    /**
     * @param string|null $url
     */
    public function setUrl(?string $url): void {
        $this->url = $url;
    }

    /**
     * @return DateTime|null
     */
    public function getDate(): ?DateTime {
        return $this->date;
    }

    /**
     * @param DateTime|null $date
     */
    public function setDate(?DateTime $date): void {
        $this->date = $date;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void {
        $this->description = $description;
    }

    /**
     * @return Collection
     */
    public function getProjectImages(): Collection {
        return $this->projectImages;
    }

    /**
     * @param Collection $projectImages
     */
    public function setProjectImages(Collection $projectImages): void {
        foreach ($projectImages as $image) {
            $this->addProjectImage($image);
        }
    }

    public function addProjectImage(ProjectImage $image) {
        $image->setProject($this);
        $this->projectImages[] = $image;
        $this->updatedAt = new DateTime('now');
    }

    public function removeProjectImage(ProjectImage $image)
    {
        $this->projectImages->removeElement($image);
        $image->setProject(null);
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setSampleImageFile(File $image = null)
    {
        $this->sampleImageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getSampleImageFile()
    {
        return $this->sampleImageFile;
    }

    public function setSampleImage($image)
    {
        $this->sampleImage = $image;
    }

    public function getSampleImage()
    {
        return $this->sampleImage;
    }

    public function getImageUrl() {
        return 'http://api.matfix.cz/images/'.$this->image;
    }

    public function getSampleImageUrl() {
        return 'http://api.matfix.cz/images/'.$this->sampleImage;
    }

    /**
     * @return string|null
     */
    public function getProjectWebUrl(): ?string {
        return $this->projectWebUrl;
    }

    /**
     * @param string|null $projectWebUrl
     */
    public function setProjectWebUrl(?string $projectWebUrl): void {
        $this->projectWebUrl = $projectWebUrl;
    }

    /**
     * @return string|null
     */
    public function getSeoTitle(): ?string {
        return $this->seoTitle;
    }

    /**
     * @param string|null $seoTitle
     */
    public function setSeoTitle(?string $seoTitle): void {
        $this->seoTitle = $seoTitle;
    }

    /**
     * @return string|null
     */
    public function getSeoDesc(): ?string {
        return $this->seoDesc;
    }

    /**
     * @param string|null $seoDesc
     */
    public function setSeoDesc(?string $seoDesc): void {
        $this->seoDesc = $seoDesc;
    }

    /**
     * @return string|null
     */
    public function getSeoKeyWords(): ?string {
        return $this->seoKeyWords;
    }

    /**
     * @param string|null $seoKeyWords
     */
    public function setSeoKeyWords(?string $seoKeyWords): void {
        $this->seoKeyWords = $seoKeyWords;
    }
}
