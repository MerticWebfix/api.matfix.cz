<?php declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TagRepository")
 * @ORM\Table()
 */
class Tag {

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $id;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @var string|null
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string|null
     */
    private $seoTitle = null;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     * @var string|null
     */
    private $seoDesc = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string|null
     */
    private $seoKeyWords = null;

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string {
        return $this->url;
    }

    /**
     * @param string|null $url
     */
    public function setUrl(?string $url): void {
        $this->url = $url;
    }

    public function __toString() {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getSeoTitle(): ?string {
        return $this->seoTitle;
    }

    /**
     * @param string|null $seoTitle
     */
    public function setSeoTitle(?string $seoTitle): void {
        $this->seoTitle = $seoTitle;
    }

    /**
     * @return string|null
     */
    public function getSeoDesc(): ?string {
        return $this->seoDesc;
    }

    /**
     * @param string|null $seoDesc
     */
    public function setSeoDesc(?string $seoDesc): void {
        $this->seoDesc = $seoDesc;
    }

    /**
     * @return string|null
     */
    public function getSeoKeyWords(): ?string {
        return $this->seoKeyWords;
    }

    /**
     * @param string|null $seoKeyWords
     */
    public function setSeoKeyWords(?string $seoKeyWords): void {
        $this->seoKeyWords = $seoKeyWords;
    }
}
