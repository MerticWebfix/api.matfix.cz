<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\ProjectImage;
use App\Entity\Tag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ProjectImageRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProjectImage::class);
    }

    public function findByTag(Tag $tag) {
        $qb = $this->createQueryBuilder('pi');
        $qb->leftJoin('pi.project', 'p');
        $qb->andWhere('pi.tag = :tag')->setParameter('tag', $tag);
        $qb->orderBy('p.date', 'DESC');

        return $qb->getQuery()->getResult();
    }
}
