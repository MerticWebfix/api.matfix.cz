<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class TagsController extends AbstractController {

    /**
     *   @Route("/api/tags", name="tags", methods={"GET","HEAD"})
     */
    public function getTags(): JsonResponse {
        $repo = $this->getDoctrine()->getRepository(Tag::class);
        /** @var $tags Tag[] */
        $tags = $repo->findAll();
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $data = [];
        foreach ($tags as $tag) {
            $tagData = [
                'name' => $tag->getName(),
                'url' => $tag->getUrl(),
                'seo' => [
                    'title' => $tag->getSeoTitle(),
                    'description' => $tag->getSeoDesc(),
                    'keywords' => $tag->getSeoKeyWords()
                ]
            ];
            $data[] = $tagData;
        }

        return new JsonResponse($data);
    }
}
