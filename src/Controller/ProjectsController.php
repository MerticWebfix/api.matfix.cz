<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\Project;
use App\Entity\ProjectImage;
use App\Entity\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ProjectsController extends AbstractController {

    /**
     * @Route("/api/projects/{tagUrl}", name="projects_by_tag", methods={"GET","HEAD"})
     */
    public function getProjectsByTag($tagUrl): JsonResponse {
        $tagRepository = $this->getDoctrine()->getRepository(Tag::class);
        $tag = $tagRepository->findOneBy(['url' => $tagUrl]);

        $data = [];
        if ($tag) {
            $repo = $this->getDoctrine()->getRepository(ProjectImage::class);
            $projectImages = $repo->findByTag($tag);

            /** @var $projectImage ProjectImage */
            foreach ($projectImages as $projectImage) {
                $project = $projectImage->getProject();
                $piData = [
                    'project' => [
                        'name' => $project->getName(),
                        'url' => $project->getUrl(),
                        'id' => $project->getId()
                    ],
                    'tag' => [
                        'url' => $tag->getUrl(),
                        'name' => $tag->getName(),
                        'seo' => [
                            'title' => $tag->getSeoTitle(),
                            'description' => $tag->getSeoDesc(),
                            'keywords' => $tag->getSeoKeyWords()
                        ]
                    ],
                    'imageUrl' => $projectImage->getSampleImageUrl()
                ];

                $data[] = $piData;
            }


        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/api/project/{url}", name="project_by_id", methods={"GET","HEAD"})
     */
    public function getProjectById($url): JsonResponse {
        $projectRepository = $this->getDoctrine()->getRepository(Project::class);

        if (is_numeric($url)) {
            $project = $projectRepository->find($url);
        } else {
            $project = $projectRepository->findOneBy(['url' => $url]);
        }

        $data = [];
        $status = Response::HTTP_NOT_FOUND;

        /** @var $project Project|null */
        if ($project) {
            $status = Response::HTTP_OK;
            $defaultContext = [
                AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                    return $object->getName();
                },
            ];
            $encoders = [new JsonEncoder()];
            $normalizers = [new ObjectNormalizer(null, null, null, null, null, null, $defaultContext), new ArrayDenormalizer()];
            $serializer = new Serializer($normalizers, $encoders);
            $data =
                [

                        'name' => $project->getName(),
                        'url' => $project->getUrl(),
                        'id' => $project->getId(),
                        'description' => $project->getDescription(),
                        'projectWebUrl' => $project->getProjectWebUrl(),
                        'mainImageUrl' => $project->getImageUrl(),
                        'seo' => [
                            'title' => $project->getSeoTitle(),
                            'description' => $project->getSeoDesc(),
                            'keywords' => $project->getSeoKeyWords()
                        ]
                ];
            $projectImagesData = [];
            /** @var $projectImage ProjectImage */
            foreach ($project->getProjectImages() as $projectImage) {
                $projectImagesData[] = [
                    'tag' => [
                        'url' => $projectImage->getTag()->getUrl(),
                        'name' => $projectImage->getTag()->getName()
                    ],
                    'imageUrl' => $projectImage->getImageUrl()
                ];
            }

            $data['projectImages'] = $projectImagesData;
        }

        return new JsonResponse($data, $status);
    }

}
