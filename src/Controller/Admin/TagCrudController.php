<?php

namespace App\Controller\Admin;

use App\Entity\Tag;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class TagCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Tag::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInPlural('Tagy')
            ->setEntityLabelInSingular('Tag')
            ;
    }

    public function configureFields(string $pageName): iterable {
        return [
            FormField::addPanel('Tag')->setIcon('data'),
            IdField::new('id')->hideOnForm(),
            TextField::new('name')->setLabel('Název'),
            TextField::new('url')->onlyOnIndex()->onlyOnDetail(),
            FormField::addPanel('SEO')->setIcon('data'),
            TextField::new('seoTitle')->setLabel('Title')->onlyOnForms(),
            TextField::new('seoDesc')->setLabel('Description')->onlyOnForms(),
            TextField::new('seoKeyWords')->setLabel('Key words')->onlyOnForms(),
        ];
    }
}
