<?php

namespace App\Controller\Admin;

use App\Admin\Fields\VichImageField;
use App\Entity\Project;
use App\FormType\ProjectImageType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ProjectCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Project::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInPlural('Projekty')
            ->setEntityLabelInSingular('Projekt')
            ->setSearchFields(['name', 'description'])
            ->setDefaultSort(['id' => 'DESC'])
            ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            FormField::addPanel('Hlavní údaje')->setIcon('list'),
            TextField::new('name')->setLabel('Název'),
            VichImageField::new('imageFile')->onlyOnForms()->setLabel('Hlavní obrázek'),
            VichImageField::new('sampleImageFile')
                ->onlyOnForms()
                ->setLabel('Náhledový obrázek')
                ->setHelp('Pokud bude vyplněný, zobrazí se na výpisu projektů místo tagovaného.'),
            TextEditorField::new('description')->hideOnIndex()->setLabel('Popis'),
            DateField::new('date')->setLabel('Datum')->setHelp('formát dd.mm.YYYY'),
            TextField::new('projectWebUrl')->setLabel('Adresa webu')->onlyOnForms(),
            FormField::addPanel('SEO')->setIcon('data'),
            TextField::new('seoTitle')->setLabel('Title')->onlyOnForms(),
            TextField::new('seoDesc')->setLabel('Description')->onlyOnForms(),
            TextField::new('seoKeyWords')->setLabel('Key words')->onlyOnForms(),
            FormField::addPanel('Obrázky')->setIcon('picture'),
            CollectionField::new('projectImages')->setLabel('Obrázky')
                ->allowAdd()->allowDelete()->onlyOnForms()->setEntryType(ProjectImageType::class)
                ->setFormTypeOption('by_reference', false)
        ];
    }

}
