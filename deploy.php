<?php

declare(strict_types=1);

namespace Deployer;

require 'recipe/symfony4.php';

// Project name
set('application', 'Sample app');

// Project repository
set('repository', 'git@bitbucket.org:MerticWebfix/api.matfix.cz.git');

// Set composer options
set('composer_options', '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --optimize-autoloader --no-scripts');
set('shared_files', []);
set('shared_dirs', ['var/log']);
set('writable_mode', 'chown');
set('writable_recursive', true);
set('http_user', 'www-data');
set('writable_dirs', ['var/log', 'var/cache', 'public']);
set('deploy_path','/www/hosting/matfix.cz/api');
set('use_atomic_symlinks', false);
inventory('hosts.yml');
// shared files & folders
//add('shared_files', ['.env.local']);


// Tasks
task('pwd', function (): void {
    $result = run('pwd');
    writeln("Current dir: {$result}");
});

// [Optional]  Migrate database before symlink new release.
// before('deploy:symlink', 'database:migrate');


task('deploy:build:assets', function (): void {
    run('{{bin/php}} {{bin/console}} assets:install public');
})->desc('Install front-end assets');

before('deploy:symlink', 'deploy:build:assets');

//// Upload assets
//task('upload:assets', function (): void {
//    upload(__DIR__.'/public/build/', '{{release_path}}/public/build');
//});

//after('deploy:build:assets', 'upload:assets');
//
// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
